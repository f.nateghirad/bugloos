<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\Controller;
use App\Models\Item;
use App\Services\Grid\GridService;

class ItemController extends Controller
{
    public $model;
    public function __construct(Item $item)
    {
        $this->model = $item;
    }

    public function index()
    {
        $columns = [
            'title' => [
                'display_name' => 'عنوان',
                'type' => 'string',
                'sortable' => true,
                'searchable' => true
            ],
            'price' => [
                'display_name' => 'قیمت',
                'type' => 'float',
                'sortable' => true
            ],
            'count' => [
                'display_name' => 'تعداد',
                'type' => 'int'
            ],
            'status' => [
                'display_name' => 'نمایش',
                'type' => 'boolean'
            ],
        ];

        [$sort, $search, $pageSize, $data, $total, $page] = GridService::handel($this->model, $columns);

        return view('admin.grid', compact('sort', 'search', 'pageSize', 'data', 'total', 'page', 'columns'));
    }
}
