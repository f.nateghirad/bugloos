<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\Controller;
use App\Services\Grid\GridService;
use Illuminate\Http\Request;

class PostController extends Controller
{
    public function index()
    {
        $columns = [
            'id' => [
                'display_name' => '#',
                'type' => 'int',
                'sortable' => true,
            ],
            'title' => [
                'display_name' => 'عنوان',
                'type' => 'string',
                'sortable' => true,
                'searchable' => true
            ],
            'body' => [
                'display_name' => 'متن',
                'type' => 'string',
            ]
        ];

        [$sort, $search, $pageSize, $data, $total, $page] = GridService::handel('http://jsonplaceholder.typicode.com/posts', $columns);;

        return view('admin.grid', compact('sort', 'search', 'pageSize', 'data', 'total', 'page', 'columns'));
    }
}
