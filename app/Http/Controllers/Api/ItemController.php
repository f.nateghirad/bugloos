<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Item;
use App\Services\Grid\GridService;

class ItemController extends Controller
{
    public $model;
    public function __construct(Item $item)
    {
        $this->model = $item;
    }

    public function index()
    {
        $columns = [
            'title' => [
                'display_name' => 'عنوان',
                'type' => 'string',
                'sortable' => true,
                'searchable' => true
            ],
            'price' => [
                'display_name' => 'قیمت',
                'type' => 'float',
                'sortable' => true
            ],
            'count' => [
                'display_name' => 'تعداد',
                'type' => 'int'
            ],
            'status' => [
                'display_name' => 'نمایش',
                'type' => 'boolean'
            ],
        ];

        [$sort, $search, $pageSize, $data, $total, $page] = GridService::handel($this->model, $columns);

        return json_response([
            "columns" => $columns,
            "pageSize" => $pageSize,
            "sort" => $sort,
            "search" => $search,
            "data" => $data,
            "total" => $total,
            "page" => $page
        ]);
    }
}
