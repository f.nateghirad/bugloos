<?php

namespace App\Services\Grid;


use Illuminate\Support\Facades\Config;

class GridService
{

    public static function handel($source, $columns = [])
    {
        [$sort, $search, $pageSize, $page] = self::requestParser($columns);

        if ($source instanceof \Illuminate\Database\Eloquent\Model) {
            $grid = new DbGridService($source, $columns, $pageSize, $sort, $search);
        } else {
            $grid = new ApiGridService($source, $columns, $pageSize, $sort, $search, $page);
        }

        [$data, $total] = $grid->handel();

        return [$sort, $search, $pageSize, $data, $total, $page];
    }

    public static function requestParser($columns) :array
    {
        $request = request();

        $pageSize = self::sizeHandler($request);

        $sort = self::sortHandler($request, $columns);

        $search = self::searchHandler($request, $columns);

        $page = self::pageHandler($request);

        return [$sort, $search, $pageSize, $page];
    }

    protected static function sortHandler($request, $columns)
    {
        $sortables = array();

        foreach ($columns as $key => $column) {
            if (isset($column['sortable']) and $column['sortable'] == true) {
                $sortables[] = $key;
            }
        }

        if ($request->has('sortBy') and in_array($request->get('sortBy'), $sortables)) {
            $sort['column'] = $request->get('sortBy');
        } else {
            $sort['column'] = '';
        }

        if ($request->has('sortDir') and in_array($request->get('sortDir'), ['asc', 'desc'])) {
            $sort['direction'] = $request->get('sortDir');
        } else {
            $sort['direction'] = 'asc';
        }

        return $sort;
    }


    public static function searchHandler($request, $columns)
    {
        $searchables = array();

        foreach ($columns as $key => $column) {
            if (isset($column['searchable']) and $column['searchable'] == true) {
                $searchables[] = $key;
            }
        }

        if ($request->has('searchBy') and in_array($request->get('searchBy'), $searchables)) {
            $search['column'] = $request->get('searchBy');
        } else {
            $search['column'] = '';
        }

        if ($request->has('searchString')) {
            $search['string'] = $request->get('searchString');
        } else {
            $search['string'] = '';
        }

        return $search;
    }

    public static function sizeHandler($request)
    {
        if ($request->has('pageSize')) {
            $pageSize = (int)$request->get('pageSize');
        } else {
            $pageSize = Config::get('grid.page_size');
        }

        return $pageSize;
    }

    public static function pageHandler($request)
    {
        if ($request->has('page')) {
            $page = (int)$request->get('page');
        } else {
            $page = 1;
        }

        return $page;
    }
}
