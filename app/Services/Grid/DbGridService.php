<?php

namespace App\Services\Grid;

class DbGridService implements GridInterface
{
    private $source;
    private $columns;
    private $pageSize;
    private $sort;
    private $search;

    public function __construct($source, $columns, $pageSize, $sort, $search)
    {
        $this->source = $source;
        $this->columns = $columns;
        $this->pageSize = $pageSize;
        $this->sort = $sort;
        $this->search = $search;
    }

    public function handel()
    {
        $select = array_keys($this->columns);

        $query = $this->source->select($select);

        if ($this->search['column'] != '' and $this->search['string'] != '') {
            $query = $query->where($this->search['column'], 'like', '%' . $this->search['string'] . '%');
        }

        if ($this->sort['column'] != '') {
            $query = $query->orderBy($this->sort['column'], $this->sort['direction']);
        }

        $result = $query->paginate($this->pageSize);

        return [$result->toArray()['data'], $result->total()];
    }
}
