<?php

namespace App\Services\Grid;

use App\Models\ApiGridModel;
use Illuminate\Support\Facades\Http;

class ApiGridService implements GridInterface
{
    private $source;
    private $columns;
    private $pageSize;
    private $sort;
    private $search;
    private $page;

    public function __construct($source, $columns, $pageSize, $sort, $search, $page)
    {
        $this->source = $source;
        $this->columns = $columns;
        $this->pageSize = $pageSize;
        $this->sort = $sort;
        $this->search = $search;
        $this->page = $page;
    }

    public function handel()
    {
        $result = $this->getData($this->source);

        $data = $this->selectColumns($result);

        $data = collect($data);

        $data = $this->handelSort($data);

        $data = $this->handelSearch($data);

        $total = count($data);

        if ($total) {
            $paginatedData = $this->pagination($data);
            return [$paginatedData, $total];
        } else {
            return [$data, $total];
        }

    }

    public function getData($source)
    {
        try {
            $response = Http::get($source);
            return json_decode($response->body(), true);
        } catch (\Exception $exception) {
            return [];
        }

    }

    public function selectColumns($result)
    {
        $select = array_keys($this->columns);
        $data = array();

        foreach ($result as $item) {
            $record = [];
            foreach ($select as $column) {
                $record[$column] = $item[$column];
            }
            $data[] = $record;
        }

        return $data;
    }

    public function handelSort($data)
    {
        if ($this->sort['column'] != '') {
            if ($this->sort['direction'] == 'asc') {
                $data = $data->sortBy('id');
            } else {
                $data = $data->sortByDesc('id');
            }
        }

        return $data;
    }

    public function handelSearch($data)
    {
        if ($this->search['column'] != '' and $this->search['string'] != '') {
            $searchData = [];
            foreach ($data as $item) {
                if (strpos($item[$this->search['column']], $this->search['string']) !== false) {
                    $searchData[] = $item;
                }
            };

            $data = collect($searchData);
        }

        return $data;
    }

    public function pagination($data)
    {
        $data = $data->chunk($this->pageSize);

        return $data[$this->page - 1];
    }
}
