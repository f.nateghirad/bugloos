<?php


function json_response($payload = [], $errors = [], $success = true, $statusCode = 200, $message = '')
{
    $jsonData = [
        'success' => $success
    ];

    if ($payload != []) {
        $jsonData['payload'] = $payload;
    }
    if ($errors != []) {
        $jsonData['errors'] = $errors;
    }

    if ($message != '') {
        $jsonData['message'] = $message;
    }

    $headers =  [];

    return response()->json($jsonData, $statusCode, $headers, JSON_UNESCAPED_UNICODE);
}

function gridPagination($total, $pageSize)
{
    $numberOfPage = $total / $pageSize;
    $size = (int)$numberOfPage;
    if ($size < $numberOfPage) {
        $size += 1;
    }

    $currentQueries = request()->query();


    $html = "<nav><ul class='pagination'>";

    for ($i = 1; $i < $size; $i++) {
        $newQueries = ['page' => $i];
        $allQueries = array_merge($currentQueries, $newQueries);

        $html .= "<li><a href='" . request()->fullUrlWithQuery($allQueries) . "'>" . $i . "</a></li>";
    }
    $html .= "</ul></nav>";

    return $html;
}
