@extends('admin.master')

@section('content')
    <!-- Secondary header -->
    <div class="header-secondary row gray-bg">
        <div class="col-lg-12">
            <div class="page-heading clearfix">
                <h1 class="page-title pull-left">نمایش لیست</h1>
            </div>
        </div>
    </div>
    <!-- /secondary header -->
    <div class="row filter-wrapper visible-box" id="filter-box">
        <div class="col-lg-12">
            <div class="filter-header">
                <button aria-label="Close" class="close toggle-filter" type="button" data-block-id="filter-box"><i class="icon-cancel"></i></button>
                <h3 class="title">فیلتر ها</h3>
            </div>
            <form class="form-inline" action="">
                <div class="form-group">
                    <label class="form-label">مرتب سازی بر اساس</label>
                    <select name="sortBy" class="select2 form-control select2-hidden-accessible" tabindex="-1" aria-hidden="true">
                        @foreach($columns as $key => $column)
                            @if(isset($column['sortable']) and $column['sortable'] == true)
                                <option value="{{ $key }}" @if($sort['column'] == $key) selected @endif>{{ $column["display_name"] }}</option>
                            @endif
                        @endforeach
                    </select>
                </div>
                <div class="form-group">
                    <label class="form-label">جهت مرتب سازی</label>
                    <select name="sortDir" class="select2 form-control select2-hidden-accessible" tabindex="-1" aria-hidden="true">
                        <option value="asc" @if($sort['direction'] == "asc") selected @endif>asc</option>
                        <option value="desc" @if($sort['direction'] == "desc") selected @endif>desc</option>
                    </select>
                </div>
                <div class="form-group">
                    <label class="form-label">جستجو بر اساس</label>
                    <select name="searchBy" class="select2 form-control select2-hidden-accessible" tabindex="-1" aria-hidden="true">
                        @foreach($columns as $key => $column)
                            @if(isset($column['searchable']) and $column['searchable'] == true)
                                <option value="{{ $key }}" @if($search['column'] == $key) selected @endif>{{ $column["display_name"] }}</option>
                            @endif
                        @endforeach
                    </select>
                </div>
                <div class="form-group">
                    <label class="form-label">عبارت جستجو</label>
                    <input name="searchString" value="{{ $search['string'] }}" class="form-control">
                </div>
                <div class="form-group">
                    <label class="form-label">نمایش در هر صفحه</label>
                    <select name="pageSize" class="select2 form-control select2-hidden-accessible" tabindex="-1" aria-hidden="true">
                        <option value="5" @if($pageSize == "5") selected @endif>5</option>
                        <option value="10" @if($pageSize == "10") selected @endif>10</option>
                        <option value="15" @if($pageSize == "15") selected @endif>15</option>
                        <option value="20" @if($pageSize == "20") selected @endif>20</option>
                    </select>
                </div>
                <div class="form-group filter-btn">
                    <button class="btn btn-default">فیلتر</button>
                </div>
            </form>
        </div>
    </div>
    <!-- Main content -->
    <div class="main-content">
        <div class="table-responsive">
            <table class="table table-users table-hover text-center">
                <thead>
                <tr>
                    @foreach($columns as $column)
                        <td>{{ $column['display_name'] . ' (' . $column['type'] . ')' }}</td>
                    @endforeach
                </tr>
                </thead>
                <tbody>
                    @foreach($data as $item)
                        <tr>
                            @foreach($columns as $key => $column)
                                <td>{{ $item[$key] }}</td>
                            @endforeach
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
        <div class="text-center">
            {!! gridPagination($total, $pageSize) !!}
        </div>
    </div>
@endsection
