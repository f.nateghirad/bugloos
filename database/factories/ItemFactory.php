<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

class ItemFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'title' => $this->faker->name,
            'price' => $this->faker->randomFloat(2, 0, 10000),
            'count' => $this->faker->randomNumber(),
            'status' => $this->faker->boolean,
        ];
    }
}
